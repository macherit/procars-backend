const assert = require('assert');
const app = require('../../src/app');

describe('\'marcas\' service', () => {
  it('registered the service', () => {
    const service = app.service('marcas');

    assert.ok(service, 'Registered the service');
  });
});
