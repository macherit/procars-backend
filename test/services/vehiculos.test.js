const assert = require('assert');
const app = require('../../src/app');

describe('\'vehiculos\' service', () => {
  it('registered the service', () => {
    const service = app.service('vehiculos');

    assert.ok(service, 'Registered the service');
  });
});
