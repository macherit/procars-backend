#!/bin/bash

export NODE_ENV="production"

pm2 stop "procarg-backend"
pm2 start run.sh --name "procarg-backend"
