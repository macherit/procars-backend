const mailer = require("feathers-mailer");
const nodemailer = require("nodemailer");

const transporter = {
  host: "procarg.com.ar", //SERVIDOR DE SALIDA EJ.: server.macherit.tk
  port: 2525,
  secure: false,
  tls: {
    rejectUnauthorized: false
  },
  auth: {
    user: "no-reply@procarg.com.ar", //MAIL CARRIER EJ.: no-reply@agencia-modelos.com
    pass: "eaor5ddl8cva"
  }
};

let app;

module.exports = {
  init: newApp => {
    app = newApp;
  },
  contacto: {
    async create(data, params) {
      try {
        if (app && data.email) {
          await nodemailer.createTestAccount(); // internet required

          // Register service and setting default From Email
          app.use(
            "mailer",
            mailer(transporter, { from: "no-reply@procarg.com.ar" })
          );

          await app.service("mailer").create({
            to: data.email,
            subject: "Contacto desde Procarg",
            html: `Gracias por comunicarte con Procarg, nos contactaremos a la brevedad.<br />Atte. Procarg`
          });

          await app.service("mailer").create({
            to: "info@procarg.com.ar",
            subject: "Contacto desde Procarg",
            html: `Se realizó una consulta desde el sitio web:<br />Nombre: ${data.nombre}<br />Email: ${data.email}<br />Teléfono: ${data.telefono}<br />Modelo de vehículo: ${data.modelo}<br />Días de uso: ${data.dias}`
          });

          return "Success";
        }
      } catch (error) {
        console.log(error);
      }
      throw new Error("Ocurrió un error");
    }
  },
  newsletters: {
    async create(data, params) {
      try {
        if (app && data.email) {
          await nodemailer.createTestAccount(); // internet required

          // Register service and setting default From Email
          app.use(
            "mailer",
            mailer(transporter, { from: "no-reply@procarg.com.ar" })
          );

          await app.service("mailer").create({
            to: data.email,
            subject: "Nueva suscripción a newsletters de Procarg",
            html: `Gracias por suscribirse a los newsletters de Procarg, nos comunicaremos a la brevedad.<br />Atte. Procarg`
          });

          await app.service("mailer").create({
            to: "info@procarg.com.ar",
            subject: "Nueva suscripción a newsletters de Procarg",
            html: `Se realizó una suscripción a newsletters desde el sitio web:<br />Email: ${data.email}`
          });

          return "Success";
        }
      } catch (error) {
        console.log(error);
      }
      throw new Error("Ocurrió un error");
    }
  }
};
