#!/bin/bash

if test -f server.tar; then
  rm server.tar
fi

tar -cvf server.tar config/ modules/ public/ src/ server-run.sh run.sh package.json yarn.lock

scp -P 7822 server.tar procarg@75.98.172.211:/home/procarg/procarg-backend/

rm server.tar

ssh procarg@75.98.172.211 -p 7822 'cd /home/procarg/procarg-backend/; tar -xvf server.tar; rm server.tar'