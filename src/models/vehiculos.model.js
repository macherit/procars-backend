// vehiculos-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const vehiculos = new Schema(
    {
      marca: {
        type: Schema.Types.ObjectId,
        ref: "marcas"
      },
      tipo: {
        type: String,
        required: true,
        default: "auto"
      },
      nombre: {
        type: String,
        required: true
      },
      descripcion: {
        type: String
      },
      velocidadMaxima: {
        type: Number
      },
      autonomia: {
        type: Number
      },
      kilometraje: {
        type: Number
      },
      fotos: {
        type: [String]
      },
      imagenPrincipal: {
        type: String
      }
    },
    {
      timestamps: true
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://github.com/Automattic/mongoose/issues/1251
  try {
    return mongooseClient.model("vehiculos");
  } catch (e) {
    return mongooseClient.model("vehiculos", vehiculos);
  }
};
