// Initializes the `marcas` service on path `/marcas`
const { Marcas } = require('./marcas.class');
const createModel = require('../../models/marcas.model');
const hooks = require('./marcas.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/marcas', new Marcas(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('marcas');

  service.hooks(hooks);
};
