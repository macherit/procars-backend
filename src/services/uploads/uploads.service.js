// Initializes the `uploads` service on path `/uploads`
const { Uploads } = require("./uploads.class");
const createModel = require("../../models/uploads.model");
const hooks = require("./uploads.hooks");

const blobService = require("feathers-blob");

const multer = require("multer");
const multipartMiddleware = multer();

module.exports = function(app) {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate")
  };

  const fs = require("fs-blob-store");
  const blobStorage = fs("./uploads");

  // Initialize our service with any options it requires
  app.use(
    "/uploads",
    multipartMiddleware.single("uri"),
    function(req, _res, next) {
      req.feathers.file = req.file;
      next();
    },
    blobService({ Model: blobStorage })
  );

  // Get our initialized service so that we can register hooks
  const service = app.service("uploads");

  service.hooks(hooks);
};
