// Initializes the `vehiculos` service on path `/vehiculos`
const { Vehiculos } = require('./vehiculos.class');
const createModel = require('../../models/vehiculos.model');
const hooks = require('./vehiculos.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/vehiculos', new Vehiculos(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('vehiculos');

  service.hooks(hooks);
};
