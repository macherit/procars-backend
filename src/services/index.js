const users = require("./users/users.service.js");
const marcas = require("./marcas/marcas.service.js");
const vehiculos = require("./vehiculos/vehiculos.service.js");
const uploads = require("./uploads/uploads.service.js");
const mailing = require("../../modules/mailing");
const clientes = require('./clientes/clientes.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function(app) {
  app.configure(users);
  app.configure(marcas);
  app.configure(vehiculos);
  app.configure(uploads);
  app.configure(clientes);

  mailing.init(app);

  app.use("/mailing/contacto", mailing.contacto);
  app.use("/mailing/newsletters", mailing.newsletters);
};
